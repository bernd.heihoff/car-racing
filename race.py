from os import error
import time
import pygame
import numpy as np

# Zustand des Autos. Details spielen keine Rolle.
car_state = {'sensors': [], 'target_velocity': 0, 'velocity': 0, 'steering': 0, 'reward': 0}



##################################################################################
# Tools
# Was jetzt kommt, sind die Interaktionsmöglichkeiten mit dem Auto.
##################################################################################

def add_sensor(relative_pos_x, relative_pos_y):
    # Fügt einen neuen Sensor hinzu.
    # Sensoren prüfen, ob ein Punkt in der Umgebung des Autos 
    # auf der Strecke liegt, oder nicht.
    # Argumente:
    #   relative_pos_x - relative x-Position. positiv = rechts vom Auto
    #   relative_pos_y - relative y-Position. positiv = vor dem Auto
    # Rückgabe:
    #   ID des Sensors

    car_state['sensors'].append([relative_pos_x, relative_pos_y, False])
    return len(car_state['sensors']) - 1

def is_sensor_on_track(id):
    # Holt den Zustand eines Sensors.  
    # Argumente:
    #   id - ID des Sensors
    # Rückgabe: 
    #   True, wenn der Sensor auf der Strecke liegt, sonst False
    
    if id not in range(len(car_state['sensors'])):
        raise error('Sensor-ID ungültig.')
    else:
        return car_state['sensors'][id][2]

def set_velocity(velocity):
    # Legt die Zielgeschwindigkeit fest, bis zu der das Auto beschleunigen 
    # oder bremsen wird. Je höher die Geschwindigkeit, desto stärker  
    # wird das Auto aus der Kurve getrieben. 
    # Argumente:
    #   velocity - Zielgeschwindigkeit zwischen 0 und 1.

    if velocity < 0 or velocity > 1:
        raise error('Geschwindigkeit ungültig.')
    else:
        car_state['target_velocity'] = velocity

def set_steering(steering):
    # Legt den Lenkeinschlag fest. 
    # Argumente:
    #   steering - Lenkeinschlag zwischen -1 (links) und 1 (rechts).
    
    if steering < -1 or steering > 1:
        raise error('Lenkeinschlag ungültig.')
    else:
        car_state['steering'] = steering

def get_reward():
    # Holt den Reward des letzten Simulationsschritts. 
    # Rückgabe:
    #   Reward als float.

    return float(car_state['reward'])

def get_current_velocity():
    # Holt die aktuelle Geschwindigkeit.
    # Rückgabe:
    #   Geschwindigkeit zwischen 0 und 1

    return car_state['velocity']



##################################################################################
# Das eigentliche Verhalten.
# Hier sollten Veränderungen vorgenommen werden, um die Rundenzeit des Autos 
# zu verbessern.
##################################################################################

# Definition der Streckensensoren
sensor_front = add_sensor(120, 0)
sensor_right = add_sensor(60, 27)
sensor_left = add_sensor(60, -27)

# Regelschleife
def control_step(dt):
    if not is_sensor_on_track(sensor_left) and is_sensor_on_track(sensor_right):
        set_steering(1)
    elif not is_sensor_on_track(sensor_right) and is_sensor_on_track(sensor_left):
        set_steering(-1)
    else:
        set_steering(0)
        
    if not is_sensor_on_track(sensor_front):
        set_velocity(0.22)
    else:
        set_velocity(0.51)







##################################################################################
# Engine
# Alles was jetzt kommt, ist unwichtig
##################################################################################

def interpolate(sequence, quantization):
    sequence = list(sequence)
    interpolation = []
    interpolation.append(np.array(sequence.pop(0), dtype=float))
    while sequence:
        prev = interpolation[-1]
        next = sequence.pop(0)
        interpolation.extend(np.linspace(prev, next, 1 + int(np.floor(np.linalg.norm(prev - next) / quantization)))[1:])        
    prev = interpolation[-1]
    next = interpolation[0]
    interpolation.extend(np.linspace(prev, next, 1 + int(np.floor(np.linalg.norm(prev - next) / quantization)))[1:-1])
    return np.array(interpolation)

def smooth(sequence, niter, eps):
    sequence = list(sequence)
    for _ in range(niter):
        for j in range(len(sequence)):
            prev = sequence[j - 1]
            curr = sequence[j]
            next = sequence[(j + 1) % len(sequence)]
            dir = (prev + next) / 2 - curr
            sequence[j] = sequence[j] + eps * dir
    return np.array(sequence)

def get_polygon(track, width):
    borders = []
    for i in range(len(track)):
        prev = track[i - 1]
        curr = track[i]
        next = track[(i + 1) % len(track)]
        tangent = next - prev
        normal = np.zeros((2,))
        normal[0] = tangent[1]
        normal[1] = -tangent[0]
        normal /= np.linalg.norm(normal)
        borders.append([curr - normal * (width / 2), curr + normal * (width / 2)])
    poly = []
    for i in range(len(borders)):
        if not is_on_track(track, borders[i][0]):
            poly.append(borders[i][0])
    for i in range(len(borders)):
        if not is_on_track(track, borders[-(i+1)][1]):
            poly.append(borders[-(i+1)][1])
    return np.array(poly)

def get_closest_track_point(track, position):
    dists = np.linalg.norm(track - position, axis=1)
    return np.argmin(dists)

def is_on_track(track, position, track_point=None):
    if track_point is None:
        track_point = get_closest_track_point(track, position)
    return np.linalg.norm(track[track_point] - position) < engine_track_width / 2

def rotate(vec, angle):
    return np.array([
        [np.cos(angle), -np.sin(angle)], 
        [np.sin(angle), np.cos(angle)]
    ]) @ vec

def unit_by_angle(angle):
    return np.array([np.cos(angle), np.sin(angle)])

def display_sev_seg(screen, state, orig, scale, col):
    onset_x = [0, 1, 1, 1, 0, 0, 0]
    onset_y = [0, 0, 1, 2, 2, 1, 1]
    dir_x = [1, 0, 0, -1, 0, 0, 1]
    dir_y = [0, 1, 1, 0, -1, -1, 0]
    for i in range(len(state)):
        if state[i]:
            onset = orig + scale * np.array([onset_x[i], onset_y[i]])
            targ = onset + scale * np.array([dir_x[i], dir_y[i]])
            pygame.draw.line(screen, col, onset, targ, int(scale / 3))

def display_digit(screen, dig, orig, scale, col):
    dig = int(dig)
    state = [
        not dig in [1, 4],
        not dig in [5, 6],
        not dig in [2],
        not dig in [1, 4, 7],
        dig in [0, 2, 6, 8],
        not dig in [1, 2, 3, 7],
        not dig in [0, 1, 7]
    ]
    return display_sev_seg(screen, state, orig, scale, col)

def display_number(screen, num, prec, orig, scale, col):
    s = str(num)
    s = max(0, 2 - len(s.split('.')[0])) * '0' + s
    s += '000'
    s = s[:-max(1, len(s.split('.')[1]) - prec)]
    onset_x = 0
    while s:
        if s[0] != '.':
            display_digit(screen, s[0], orig + [onset_x, 0], scale, col)
            s = s[1:]
            onset_x += 1.5 * scale
        else:
            s = s[1:]
            onset_x += 0.1 * scale
            pygame.draw.line(screen, col, orig + [onset_x, 2 * scale], orig + [onset_x + int(scale / 3), 2 * scale], int(scale / 3))
            onset_x += 0.4 * scale + int(scale / 3)
        
pygame.init()

engine_screen = pygame.display.set_mode([1100, 700])

engine_track_orig = 6 * np.array([
    [0, 50], [0, 100], [30, 100], [30, 20], [70, 30], [40, 60], [70, 90], [70, 100], [120, 100], 
    [130, 80], [80, 50], [120, 20], [110, 10], [90, 5], [60, 5], [10, 0], [0, 10]
]) + [60, 40]

engine_track_width = 50

engine_track = interpolate(engine_track_orig, 4)
engine_track = smooth(engine_track, 300, 0.5)
engine_track = np.append(engine_track, engine_track[:1], axis=0)
engine_track_progress = np.tile(np.cumsum(
    np.linalg.norm(engine_track - engine_track[list(range(1, len(engine_track))) + [0]], axis=1)
),2)
engine_track_progress[:len(engine_track)] -= engine_track_progress[len(engine_track) - 1]
engine_polygon = get_polygon(engine_track, engine_track_width)

# wer diese Zahlen verändert, schummelt :D
engine_VMAX = 500
engine_ACC = 0.2
engine_ACC_BR = 2 * engine_ACC
engine_MAX_STEER_VMAX_FACTOR = 0.07
engine_MAX_STEER = 1

engine_car_length = 20
engine_pos = [engine_track[0][0],engine_track[0][1] - 1 * engine_car_length, 0.5 * np.pi]
car_state['velocity'] = 0

engine_t = time.time()
engine_laps = [0]
engine_lap_avg = None

engine_t_start = time.time()

engine_prev_track_segment = 0
engine_round_segments = set()

def simulation_step(dt):
    global engine_t, engine_t_start, engine_pos, engine_prev_track_segment, engine_laps, engine_round_segments, engine_lap_avg

    if abs(car_state['velocity'] - car_state['target_velocity']) <= dt * engine_ACC:
        car_state['velocity'] = car_state['target_velocity']
    else:
        if car_state['target_velocity'] - car_state['velocity'] > 0:
            car_state['velocity'] += dt * engine_ACC
        else:
            car_state['velocity'] -= dt * engine_ACC_BR
        
    engine_pos[:2] += dt * engine_VMAX * unit_by_angle(engine_pos[2]) * car_state['velocity']
    actual_steering = engine_MAX_STEER / (1 + (1 / engine_MAX_STEER_VMAX_FACTOR - 1) * car_state['velocity']) * car_state['steering']
    engine_pos[2] += np.arctan(dt * engine_VMAX * car_state['velocity'] * actual_steering / engine_car_length)
    engine_pos[2] = engine_pos[2] % (2 * np.pi)

    car_nose = engine_pos[:2] + rotate([20, 0], engine_pos[2])
    curr_track_point = get_closest_track_point(engine_track, car_nose)

    curr_track_segment = int(curr_track_point / len(engine_track) * 100)
    car_state['reward'] = max(0, curr_track_segment - engine_prev_track_segment)
    finish = False
    if curr_track_segment == 0 and engine_prev_track_segment == 99 and len(engine_round_segments) >= 99:
        finish = True
        car_state['reward'] = 100
        engine_round_segments = set()
    engine_round_segments.add(curr_track_segment)
    engine_prev_track_segment = curr_track_segment
    off_track = not is_on_track(engine_track, car_nose, track_point=curr_track_point)
    if (off_track):
        car_state['reward'] = -10
        time.sleep(2)
        engine_t = time.time()

    if finish and len(engine_laps) < 3:
        engine_laps.append(engine_t)
        if len(engine_laps) > 1:
            engine_lap_avg = 0
            for i in range(len(engine_laps) - 1):
                engine_lap_avg += engine_laps[i + 1] - engine_laps[i]
            engine_lap_avg /= len(engine_laps) - 1
        if len(engine_laps) == 3:
            f = open("history.txt", "a")
            f.write("{:.3f}".format(engine_lap_avg) + '\n')
            f.close()

    sensors = car_state['sensors']
    sensors_bbox = np.tile(engine_pos[:2] + rotate(sensors[0][:2], engine_pos[2]), (2,1))
    for sensor in sensors:
        sensor_transformed = engine_pos[:2] + rotate(sensor[:2], engine_pos[2])
        sensors_bbox[0] = np.minimum(sensors_bbox[0], sensor_transformed)
        sensors_bbox[1] = np.maximum(sensors_bbox[1], sensor_transformed)
    sensors_bbox[0] -= engine_track_width / 2
    sensors_bbox[1] += engine_track_width / 2
    track_selection = engine_track[np.logical_and(np.all(engine_track >= sensors_bbox[0], axis=1), np.all(engine_track <= sensors_bbox[1],axis=1))]
    for sensor in sensors:
        sensor[2] = is_on_track(track_selection, engine_pos[:2] + rotate(sensor[:2], engine_pos[2]))
    
    # Rendering

    engine_screen.fill((255, 255, 255))

    pygame.draw.polygon(engine_screen, (100, 100, 100), engine_polygon)

    segs = np.linspace(engine_track[0] - [engine_track_width / 2, 0], engine_track[0] + [engine_track_width / 2, 0], 8)
    for i in range(len(segs) // 2):
        pygame.draw.line(engine_screen, (0,0,0), segs[2 * i], segs[2 * i + 1], 5)
    for i in range(len(segs) // 2):
        pygame.draw.line(engine_screen, (255,255,255), segs[2 * i] + [0,5], segs[2 * i + 1] + [0,5], 5)
    for i in range(len(segs) // 2 - 1):
        pygame.draw.line(engine_screen, (255,255,255), segs[2 * i + 1], segs[2 * i + 2], 5)
    for i in range(len(segs) // 2 - 1):
        pygame.draw.line(engine_screen, (0,0,0), segs[2 * i + 1] + [0,5], segs[2 * i + 2] + [0,5], 5)

    speedometer_orig = np.array([970, 250])
    speedometer_width = 7
    pygame.draw.circle(engine_screen, (200,200,200), speedometer_orig, 60)
    pygame.draw.rect(engine_screen, (255,255,255), pygame.Rect(speedometer_orig + rotate(np.array([-60, 0]), -0.25 * np.pi), [100, 20]))
    pygame.draw.line(engine_screen, (0,0,0), speedometer_orig, speedometer_orig + rotate(np.array([-50, 0]), 1.5 * np.pi * car_state['velocity'] - 0.25 * np.pi), speedometer_width)
    pygame.draw.circle(engine_screen, (0,0,0), speedometer_orig, speedometer_width)
    pygame.draw.circle(engine_screen, (0,0,0), speedometer_orig + rotate(np.array([-50, 0]), 1.5 * np.pi * car_state['velocity'] - 0.25 * np.pi), speedometer_width / 2)     

    for i in range(min(len(engine_laps), 2)):
        if i == len(engine_laps) - 1:
            display_number(engine_screen, engine_t - engine_laps[i], 3, speedometer_orig + [-55, 80] + [0, 50 * i], 14, (0,0,0))
        else:
            display_number(engine_screen, engine_laps[i + 1] - engine_laps[i], 3, speedometer_orig + [-55, 80] + [0, 50 * i], 14, (0,0,0))

    if len(engine_laps) == 3:
        display_number(engine_screen, engine_lap_avg, 3, speedometer_orig + [-55, 80 + 50 * 2.3], 14, (0,0,0))
        pygame.draw.circle(engine_screen, (0,0,0), speedometer_orig + [-80, 96] + [0, 50 * 2.3], 15, 4)
        pygame.draw.line(engine_screen, (0,0,0), speedometer_orig + [-80, 96 + 50 * 2.3] + [-15,15], speedometer_orig + [-80, 96 + 50 * 2.3] + [15,-15], 4)

    car = np.array([
        [0, -5],
        [0, 5],
        [20, 5],
        [20, -5],
    ])
    pygame.draw.polygon(engine_screen, (255, 80, 0), engine_pos[:2] + rotate(car.T, engine_pos[2]).T)
    for sensor in sensors:
        pygame.draw.circle(engine_screen, (0,sensor[2] * 255,(1 - sensor[2]) * 255), engine_pos[:2] + rotate(sensor[:2], engine_pos[2]), 3)

    pygame.display.flip()
    
    if off_track:
        engine_laps = [0]
        engine_t_start = time.time()
        engine_pos = [engine_track[0][0],engine_track[0][1] - 1 * engine_car_length, 0.5 * np.pi]
        car_state['target_velocity'] = 0
        car_state['velocity'] = 0

while True:
    engine_running = True
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            engine_running = False
    if not engine_running:
        break

    engine_t_new = time.time()
    engine_dt = engine_t_new - engine_t
    engine_t = engine_t_new
    
    simulation_step(engine_dt)

    if engine_t - engine_t_start > 1:
        control_step(engine_dt)
    else:
        engine_laps[0] = engine_t

pygame.quit()
